%-----------------------------
% Grupo 47
% Jose Lopes 83493
% Joao Freitas 83478
%-----------------------------

%-----------------------------
% movs_possiveis /4
% Este predicado, dado um labirinto os movs efetuados
% e a posicao atual devolve os movimentos possiveis a
% partir da posicao atual. Foram criados predicados auxiliares
% que eliminam os movimentos ja efetuados da resposta e e outros
% para criar os movimentos possiveis
%-----------------------------
movs_possiveis(Lab, (L_act, C_act) , Movs, Poss) :- 
	nth1(L_act, Lab, Linha_lab),
	nth1(C_act, Linha_lab, Lab_pos_atual),
	movs_possiveis_c(Lab_pos_atual, Novo_mov_c, L_act, C_act),
	movs_possiveis_b(Lab_pos_atual, Novo_mov_b, L_act, C_act),
	movs_possiveis_e(Lab_pos_atual, Novo_mov_e, L_act, C_act),
	movs_possiveis_d(Lab_pos_atual, Novo_mov_d, L_act, C_act),
	append([Novo_mov_c, Novo_mov_b, Novo_mov_e, Novo_mov_d], Novos_movs),
	elimina_movimentos_anteriores(Novos_movs, Movs, Poss).



elimina_movimentos_anteriores(Novos_movs ,Movs, Movs_poss):- 
	elimina_movimentos_anteriores(Novos_movs, Movs, Movs_poss, []).

elimina_movimentos_anteriores([],_, Movs, Movs).

elimina_movimentos_anteriores([(_,L,C)|R], Movs, Movs_poss, L_Aux):-
	member((_,L,C), Movs),
	!,
	elimina_movimentos_anteriores(R, Movs, Movs_poss, L_Aux).

elimina_movimentos_anteriores([(D,L,C)|R], Movs, Movs_poss, L_Aux):-
	!,
	append(L_Aux, [(D,L,C)], L_Aux2),
	elimina_movimentos_anteriores(R, Movs, Movs_poss, L_Aux2).



movs_possiveis_c(Lab_pos_atual, Novo_mov_c,_,_):- 
	member('c', Lab_pos_atual), 
	!,
	Novo_mov_c = [].

movs_possiveis_c(_, Novo_mov_c, L1, C1):- 
	!,
	L2 is L1 - 1,
	Novo_mov_c = [('c',L2, C1)].

movs_possiveis_b(Lab_pos_atual, Novo_mov_b,_,_):- 
	member('b', Lab_pos_atual),
	!,
	Novo_mov_b = [].

movs_possiveis_b(_, Novo_mov_b, L1, C1):- 
	!,
	L2 is L1 + 1,
	Novo_mov_b = [('b',L2, C1)].

movs_possiveis_e(Lab_pos_atual, Novo_mov_e,_,_):- 
	member('e', Lab_pos_atual),
	!,
	Novo_mov_e = [].

movs_possiveis_e(_, Novo_mov_e, L1, C1):- 
	!,
	C2 is C1 - 1,
	Novo_mov_e = [('e',L1, C2)].

movs_possiveis_d(Lab_pos_atual, Novo_mov_d,_,_):- 
	member('d', Lab_pos_atual),
	!,
	Novo_mov_d = [].

movs_possiveis_d(_, Novo_mov_d, L1, C1):- 
	!,
	C2 is C1 + 1,
	Novo_mov_d = [('d',L1, C2)].

%-----------------------------
% distancia /3
% Predicado que dadas duas posicoes devolve
% a distancia entre elas.
%-----------------------------
distancia((L1,C1), (L2,C2), Dist):-
	Dist is abs(L1-L2) + abs(C1-C2).



%-----------------------------
% ordena_poss /4
% Predicado que dado uma lista de movimentos, uma posicao,
% a posicao inicial e final de um labirinto ordena a lista
% por menor distancia do fim do labirinto, e pelo
% inicio do labirinto em caso de empate. Usa o predicado
% "distancia" ja definido.
%-----------------------------
ordena_poss([P|R], Poss_ord, Pos_inicial, Pos_final):-
	ordena_poss(R, Poss_ord, Pos_inicial, Pos_final, [P], []).

ordena_poss([], Poss_ord,_,_, Poss_ord,_).

ordena_poss([E|R], Poss_ord, Pos_inicial, Pos_final, [], Aux1):-
	append(Aux1, [E], Aux2),
	ordena_poss(R, Poss_ord, Pos_inicial, Pos_final, Aux2, []).

ordena_poss([(D1,L1,C1)|R1], Poss_ord, Pos_inicial, Pos_final, [(D2,L2,C2)|R2], Aux1):-
	distancia((L1,C1), Pos_final, Dist1),
	distancia((L2,C2), Pos_final, Dist2),
	Dist1 > Dist2,
	!,
	append(Aux1, [(D2,L2,C2)], Aux2),
	ordena_poss([(D1,L1,C1)|R1], Poss_ord, Pos_inicial, Pos_final, R2, Aux2).

ordena_poss([(D1,L1,C1)|R1], Poss_ord, Pos_inicial, Pos_final, [(D2,L2,C2)|R2], Aux1):-
	distancia((L1,C1), Pos_final, Dist1),
	distancia((L2,C2), Pos_final, Dist2),
	Dist1 < Dist2,
	!,
	append(Aux1, [(D1,L1,C1), (D2,L2,C2)|R2], Aux2),
	ordena_poss(R1, Poss_ord, Pos_inicial, Pos_final, Aux2, []).

ordena_poss([(D1,L1,C1)|R1], Poss_ord ,Pos_inicial, Pos_final, [(D2,L2,C2)|R2], Aux1):-
	distancia((L1,C1), Pos_inicial, Dist1),
	distancia((L2,C2), Pos_inicial, Dist2),
	Dist1 > Dist2,
	!,
	append(Aux1, [(D1,L1,C1),(D2,L2,C2)|R2], Aux2),
	ordena_poss(R1, Poss_ord, Pos_inicial, Pos_final, Aux2, []).

ordena_poss([(D1,L1,C1)|R1], Poss_ord, Pos_inicial, Pos_final, [(D2,L2,C2)|R2],Aux1):-
	distancia((L1,C1), Pos_inicial, Dist1),
	distancia((L2,C2), Pos_inicial, Dist2),
	Dist1 =< Dist2,
	!,
	append(Aux1, [(D2,L2,C2),(D1,L1,C1)|R2], Aux2),
	ordena_poss(R1, Poss_ord, Pos_inicial, Pos_final, Aux2, []).



%-----------------------------
% resolve1 /4
% Predicado que dado um labirinto, as posicoes inicial e final
% e os movimentos ja efetuados devolve os movimentos para a
% solucao do labirinto. Usa-se como auxilio o predicado 
% "movs_possiveis" ja definido. A solucao nunca apresenta 
% movimentos em que se passe duas vezes pela mesma celula.
% O predicado testa a direcao sempre pela mesma ordem:
% cima, baixo, esquerda, direita, e usa primeiro movimento 
% que for possivel.
%-----------------------------
resolve1(Lab, (Li,Ci), Pos_final, Movs):-
	movs_possiveis(Lab, (Li,Ci), [], Movs_poss),
	resolve1(Lab, (Li,Ci), Pos_final, Movs, [(i,Li,Ci)], Movs_poss).

resolve1(_,_, (Lf,Cf), Movs, Aux_Movs, [(Df,Lf,Cf)|_]):-
	append(Aux_Movs, [(Df,Lf,Cf)], Movs).

resolve1(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs ,[(Dp,Lp,Cp)|_]):-
	append(Aux_Movs, [(Dp,Lp,Cp)], Aux_Movs2),
	movs_possiveis(Lab, (Lp,Cp), Aux_Movs2, Movs_poss),
	resolve1(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs2, Movs_poss).

resolve1(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs, [_|R]):-
	resolve1(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs, R).



%-----------------------------
% resolve2 /4
% Predicado que dado um labirinto, as posicoes inicial e final
% e os movimentos ja efetuados devolve os movimentos para a
% solucao do labirinto. Usa-se como auxilio os predicados 
% "movs_possiveis" e "ordena_poss" ja definidos. 
% Este predicado escolhe os movimentos primeiro pelo movimento 
% mais perto do fim, em caso de empate o que se distancia mais
% do inicio, e se ainda em caso de empate pela ordem:
% cima, baixo, esquerda direita.
%-----------------------------
resolve2(Lab, (Li,Ci), Pos_final, Movs):-
	movs_possiveis(Lab, (Li,Ci), [], Movs_poss),
	ordena_poss(Movs_poss, Movs_ord, (Li,Ci), Pos_final),
	resolve2(Lab, (Li,Ci), Pos_final, Movs, [(i,Li,Ci)], Movs_ord).

resolve2(_,_, (Lf,Cf), Movs, Aux_Movs, [(Df,Lf,Cf)|_]):-
	append(Aux_Movs, [(Df,Lf,Cf)], Movs).

resolve2(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs, [(Dp,Lp,Cp)|_]):-
	append(Aux_Movs, [(Dp,Lp,Cp)], Aux_Movs2),
	movs_possiveis(Lab, (Lp,Cp), Aux_Movs2, Movs_poss),
	ordena_poss(Movs_poss, Movs_ord, Pos_inicial, Pos_final),
	resolve2(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs2, Movs_ord).

resolve2(Lab, Pos_inicial, Pos_final, Movs,Aux_Movs, [_|R]):-
	resolve2(Lab, Pos_inicial, Pos_final, Movs, Aux_Movs, R).





